package org.library.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.library.server.model.dto.AuthorCreateDTO;
import org.library.server.model.dto.AuthorDTO;
import org.library.server.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthorController.class)
class AuthorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorService authorService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void create_author_should_succeed() throws Exception {

        AuthorCreateDTO authorCreateDTO = new AuthorCreateDTO();

        authorCreateDTO.setFirstName("ד");
        authorCreateDTO.setLastName("לון");

        AuthorDTO authorDTO = new AuthorDTO();

        authorDTO.setId(1);
        authorDTO.setFirstName(authorCreateDTO.getFirstName());
        authorDTO.setLastName(authorCreateDTO.getLastName());

        when(this.authorService.addAuthor(any(AuthorCreateDTO.class))).thenReturn(authorDTO);

        RequestBuilder request = MockMvcRequestBuilders.post("/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsBytes(authorCreateDTO));

        this.mockMvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(Matchers.greaterThan(0)))
                .andExpect(jsonPath("$.firstName").value(authorCreateDTO.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(authorCreateDTO.getLastName()));
    }

    @Test
    void create_author_should_fail() throws Exception {

        AuthorCreateDTO authorCreateDTO = new AuthorCreateDTO();

        authorCreateDTO.setFirstName("ד".repeat(51));
        authorCreateDTO.setLastName("לון");

         RequestBuilder request = MockMvcRequestBuilders.post("/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsBytes(authorCreateDTO));

        this.mockMvc.perform(request).andExpect(status().isBadRequest());
    }
}