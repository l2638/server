package org.library.server.model.entity;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public class BaseCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "code")
    private Integer code;

    @Column(name = "value")
    private String value;
}
