package org.library.server.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "user_status_cd")
public class UserStatusCode extends BaseCode {
}
