package org.library.server.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "book_copy_status_cd")
public class BookCopyStatusCode extends BaseCode {
}
