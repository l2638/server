package org.library.server.model.mapper;

import org.library.server.model.dto.BorrowDTO;
import org.library.server.model.entity.Borrow;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BorrowMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "bookCopy.book.id", target = "bookId")
    BorrowDTO borrowToBorrowDTO(Borrow borrow);
}
