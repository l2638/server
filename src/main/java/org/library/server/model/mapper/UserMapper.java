package org.library.server.model.mapper;

import org.library.server.model.dto.UserCreateDTO;
import org.library.server.model.dto.UserPreviewDTO;
import org.library.server.model.entity.User;
import org.library.server.model.entity.UserStatusCode;
import org.library.server.model.enums.UserStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserPreviewDTO userToUserPreviewDTO(User user);

    List<UserPreviewDTO> usersToUserPreviewDTO(List<User> users);

    @Mapping(source = "status.value", target = "code")
    UserStatusCode userStatusToUserStatusCode(UserStatus status);

    @Mapping(target = "status.code", expression = "java(1)")
    User userCreateDTOToUser(UserCreateDTO userDetails);
}
