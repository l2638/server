package org.library.server.model.mapper;

import org.library.server.model.enums.BookCopyStatus;
import org.library.server.model.entity.BookCopyStatusCode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BookCopyMapper {

    @Mapping(source = "status.value", target = "code")
    BookCopyStatusCode bookCopyStatusToBookCopyStatusCode(BookCopyStatus status);
}
