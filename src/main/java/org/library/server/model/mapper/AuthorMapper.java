package org.library.server.model.mapper;

import org.library.server.model.dto.AuthorCreateDTO;
import org.library.server.model.dto.AuthorDTO;
import org.library.server.model.dto.AuthorDetailedDTO;
import org.library.server.model.entity.Author;
import org.library.server.model.projection.AuthorBookCountProjection;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    Author authorCreateDTOtoAuthor(AuthorCreateDTO dto);

    AuthorDTO authorToAuthorDTO(Author author);

    List<AuthorDTO> authorsToAuthorDTOs(List<Author> authors);

    default String toFullName(Author author) {
        return author.getFullName();
    }

    AuthorDetailedDTO authorBookCountProjectionToAuthorDetailedDTO(AuthorBookCountProjection author);

    List<AuthorDetailedDTO> authorBookCountProjectionsToAuthorDetailedDTOs(List<AuthorBookCountProjection> authors);
}
