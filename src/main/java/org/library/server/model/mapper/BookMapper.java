package org.library.server.model.mapper;

import org.library.server.model.dto.BookCreateDTO;
import org.library.server.model.dto.BookDTO;
import org.library.server.model.dto.BookDetailedDTO;
import org.library.server.model.dto.BookPreviewDTO;
import org.library.server.model.entity.Book;
import org.library.server.model.entity.User;
import org.library.server.model.projection.BookCountProjection;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring", uses = AuthorMapper.class)
public interface BookMapper {

    BookPreviewDTO bookToBookPreviewDTO(Book book);

    BookDTO bookCountProjectionToBookDTO(BookCountProjection projection);

    @Mapping(target = "copiesCount", expression = "java(book.getCopies().size())")
    BookDTO bookToBookDTO(Book book);

    List<BookPreviewDTO> booksToBookPreviewsDTOs(List<Book> books);

    @Mapping(source = "book.id", target = "id")
    BookDetailedDTO bookToBookDetailedDTO(Book book, User user);

    List<BookDTO> bookCountProjectionsToBookDTO(List<BookCountProjection> books);

    @Mapping(target = "author", ignore = true)
    Book bookCreateDTOToBook(BookCreateDTO dto);

    @AfterMapping
    default BookDetailedDTO setActiveAndStarred(Book book, User user, @MappingTarget BookDetailedDTO dto) {

        dto.setActive(book.getCopies().stream()
                .anyMatch(copy -> copy.getCurrentBorrow() != null && copy.getCurrentBorrow().getUser().equals(user)));

        dto.setStarred(user.getStarredBooks().contains(book));

        return dto;
    }
}
