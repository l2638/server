package org.library.server.model.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum BookCopyStatus {

    Borrowed(1),
    AvailableToBorrow(2);

    public final int value;
}
