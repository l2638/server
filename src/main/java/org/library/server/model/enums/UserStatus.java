package org.library.server.model.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserStatus {

    Active(1),
    Inactive(2);

    public final int value;
}
