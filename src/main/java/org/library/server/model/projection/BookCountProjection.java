package org.library.server.model.projection;

import org.library.server.model.entity.Author;

public interface BookCountProjection {

    Integer getId();

    void setId(final Integer id);

    String getTitle();

    Author getAuthor();

    void setAuthor(final Author author);

    int getCopiesCount();
}
