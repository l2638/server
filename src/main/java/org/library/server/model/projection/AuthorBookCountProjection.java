package org.library.server.model.projection;

public interface AuthorBookCountProjection {

    Integer getId();

    String getFirstName();

    String getLastName();

    int getBooksCount();
}
