package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
public class BookUpdateDTO {

    @NotNull(message = "Author cannot be null")
    private BookAuthorCreateDTO author;

    @NotNull(message = "Number of copies cannot be null")
    @PositiveOrZero(message = "Number of copies cannot be less than 0")
    private int copiesCount;
}
