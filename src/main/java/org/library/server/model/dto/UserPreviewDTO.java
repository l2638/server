package org.library.server.model.dto;

import lombok.Data;

@Data
public class UserPreviewDTO {

    private int id;

    private String firstName;

    private String lastName;
}