package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class AuthorUpdateDTO {

    @NotBlank(message = "First name is mandatory")
    @Size(max = 50, message = "First name cannot be grater than 50 characters")
    private String firstName;

    @NotBlank(message = "Last name is mandatory")
    @Size(max = 50, message = "Last name cannot be grater than 50 characters")
    private String lastName;
}
