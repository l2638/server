package org.library.server.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Positive;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookAuthorCreateDTO extends AuthorCreateDTO {

    @Nullable
    @Positive(message = "Id has to be greater than 0")
    private Integer id;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;
}
