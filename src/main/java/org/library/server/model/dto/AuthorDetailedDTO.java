package org.library.server.model.dto;

import lombok.Data;

@Data
public class AuthorDetailedDTO {

    private int id;

    private String firstName;

    private String lastName;

    private int booksCount;
}
