package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class BorrowEndDTO {

    @Positive(message = "Book id has to be greater than 0")
    private int bookId;

    @Positive(message = "User id has to be greater than 0")
    private int userId;
}
