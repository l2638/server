package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class StarredBookCreateDTO {

    @Positive(message = "User id must be greater than zero")
    private int userId;

    @Positive(message = "Book id must be greater than zero")
    private int bookId;
}
