package org.library.server.model.dto;

import lombok.Data;

@Data
public class BookDTO {

    private int id;

    private String title;

    private AuthorDTO author;

    private int copiesCount;
}
