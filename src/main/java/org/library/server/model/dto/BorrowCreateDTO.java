package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class BorrowCreateDTO {

    @NotNull(message = "User Id cannot be null")
    @Positive(message = "User Id has to be greater than 0")
    private Integer userId;

    @NotNull(message = "Book id cannot be null")
    @Positive(message = "Book id has to be greater than 0")
    private Integer bookId;
}
