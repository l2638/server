package org.library.server.model.dto;

import lombok.Data;

@Data
public class BookPreviewDTO {

    private int id;

    private String title;
}
