package org.library.server.model.dto;

import lombok.Data;

@Data
public class AuthorDTO {

    private int id;

    private String firstName;

    private String lastName;
}
