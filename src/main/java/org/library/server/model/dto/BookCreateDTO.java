package org.library.server.model.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class BookCreateDTO {

    @NotNull(message = "Author cannot be null")
    private BookAuthorCreateDTO author;

    @NotBlank(message = "Title is mandatory")
    @Size(max = 100, message = "Title cannot be greater than 100 characters")
    private String title;

    @NotNull(message = "Number of copies cannot be null")
    @PositiveOrZero(message = "Number of copies cannot be less than 0")
    private int copiesCount;
}
