package org.library.server.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserCreateDTO extends UserUpdateDTO {

    @NotNull(message = "Id cannot be null")
    @Min(value = 10, message = "Id has to be at least 10")
    @Max(value = 999999999, message = "Id cannot be greater than 999999999")
    private int id;
}
