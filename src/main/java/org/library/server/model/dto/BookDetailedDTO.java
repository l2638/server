package org.library.server.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BookDetailedDTO {

    private int id;

    private String title;

    private String author;

    @JsonProperty("isStarred")
    private boolean isStarred;

    @JsonProperty("isActive")
    private boolean isActive;
}
