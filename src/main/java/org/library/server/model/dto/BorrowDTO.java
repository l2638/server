package org.library.server.model.dto;

import lombok.Data;

@Data
public class BorrowDTO {

    private int id;

    private int bookId;

    private int userId;
}
