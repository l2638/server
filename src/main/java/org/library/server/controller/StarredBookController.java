package org.library.server.controller;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.StarredBookCreateDTO;
import org.library.server.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

@RestController
@RequestMapping("starredBooks")
@RequiredArgsConstructor
@Validated
public class StarredBookController {

    private final UserService userService;

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping()
    public void removeStar(@RequestParam @Positive int userId,
                           @RequestParam @Positive int bookId) throws ResourceNotFoundException {
        this.userService.removeStar(userId, bookId);
    }

    @PostMapping()
    public void addStar(@RequestBody @Valid StarredBookCreateDTO starDetails) throws ResourceNotFoundException {
        this.userService.addStar(starDetails.getUserId(), starDetails.getBookId());
    }

}
