package org.library.server.controller;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.*;
import org.library.server.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping()
    public List<BookDTO> getBooks() {
        return this.bookService.getBooks();
    }

    @GetMapping("/previews")
    public List<BookPreviewDTO> getBookPreviews(@RequestParam int authorId) {
        return this.bookService.getBookPreviews(authorId);
    }

    @PutMapping("/{id}")
    public BookDTO updateBook(@PathVariable int id, @Valid @RequestBody BookUpdateDTO book) throws Exception {
        return this.bookService.updateBook(id, book);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public BookDTO addBook(@Valid @RequestBody BookCreateDTO book) throws IllegalActionException, ResourceNotFoundException {
        return this.bookService.addBook(book);
    }

    @GetMapping("/available")
    public List<BookPreviewDTO> getAvailableBooks() {
        return this.bookService.getAvailableBooks();
    }

    @GetMapping(params = "userId")
    public List<BookDetailedDTO> getBooksByUser(@RequestParam int userId) throws ResourceNotFoundException {
        return this.bookService.getBooksByUser(userId);
    }
}
