package org.library.server.controller;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.AuthorCreateDTO;
import org.library.server.model.dto.AuthorDTO;
import org.library.server.model.dto.AuthorDetailedDTO;
import org.library.server.model.dto.AuthorUpdateDTO;
import org.library.server.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping("/previews")
    public List<AuthorDTO> getAuthorsPreviews() {
        return this.authorService.getAuthors();
    }

    @GetMapping()
    public List<AuthorDetailedDTO> getAuthors() {
        return this.authorService.getAuthorsDetailed();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeAuthor(@PathVariable int id) throws ResourceNotFoundException, IllegalActionException {
        this.authorService.removeAuthor(id);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public AuthorDTO addAuthor(@Valid @RequestBody AuthorCreateDTO author) {
        return this.authorService.addAuthor(author);
    }

    @PutMapping("{id}")
    public AuthorDTO updateAuthor(@PathVariable int id, @Valid @RequestBody AuthorUpdateDTO author) throws ResourceNotFoundException {
        return this.authorService.updateAuthor(id, author);
    }

}
