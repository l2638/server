package org.library.server.controller;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.BorrowCreateDTO;
import org.library.server.model.dto.BorrowDTO;
import org.library.server.model.dto.BorrowEndDTO;
import org.library.server.service.BorrowService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("borrows")
@RequiredArgsConstructor
public class BorrowController {

    private final BorrowService borrowService;

    @PostMapping()
    public BorrowDTO addBorrow(@Valid @RequestBody BorrowCreateDTO borrow) throws ResourceNotFoundException, IllegalActionException {
        return this.borrowService.addBorrow(borrow);
    }

    @PutMapping("/end")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void endBorrow(@Valid @RequestBody BorrowEndDTO borrowDetails) throws ResourceNotFoundException {
        this.borrowService.endBorrow(borrowDetails);
    }
}