package org.library.server.controller;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.UserCreateDTO;
import org.library.server.model.dto.UserPreviewDTO;
import org.library.server.model.dto.UserUpdateDTO;
import org.library.server.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping()
    public List<UserPreviewDTO> getUsers() {
        return this.userService.getUsers();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeUser(@PathVariable int id) throws ResourceNotFoundException {
        this.userService.removeUser(id);
    }

    @PutMapping("/{id}")
    public UserPreviewDTO updateUser(@PathVariable int id, @Valid @RequestBody UserUpdateDTO user) throws ResourceNotFoundException {
        return this.userService.updateUser(id, user);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public UserPreviewDTO addUser(@Valid @RequestBody UserCreateDTO user) throws IllegalActionException {
        return this.userService.addUser(user);
    }

    @GetMapping(params = "bookId")
    public List<UserPreviewDTO> getUsersByBook(@RequestParam int bookId) {
        return this.userService.getUsersByBook(bookId);
    }
}
