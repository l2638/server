package org.library.server.utility;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExtensions {

    public static <T> List<T> toList(Stream<T> stream) {
        return stream.collect(Collectors.toList());
    }
}
