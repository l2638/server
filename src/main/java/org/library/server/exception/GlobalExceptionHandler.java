package org.library.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> constraintViolationException(
            ConstraintViolationException ex,
            WebRequest request) {
        return this.getResponse(request, HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<?> httpMessageNotReadableException(
            HttpMessageNotReadableException ex,
            WebRequest request) {
        return this.getResponse(request, HttpStatus.BAD_REQUEST, "Cannot read request body");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> methodArgumentNotValidException(
            MethodArgumentNotValidException ex,
            WebRequest request) {

        String message = ex.getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.joining(", "));

        return this.getResponse(request, HttpStatus.BAD_REQUEST, message);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        return this.getResponse(request, HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(IllegalActionException.class)
    public ResponseEntity<?> illegalActionException(IllegalActionException ex, WebRequest request) {
        return this.getResponse(request, HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    private ResponseEntity<?> getResponse(
            WebRequest request,
            HttpStatus status,
            String message) {

        Map<String, String> details = new LinkedHashMap<>() {{
            this.put("timestamp", LocalDateTime.now().toString());
            this.put("error", status.getReasonPhrase());
            this.put("message", message);
            this.put("path", request.getDescription(false));
        }};

        return new ResponseEntity<>(details, status);
    }
}
