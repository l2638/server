package org.library.server.exception;

public class IllegalActionException extends Exception {

    private static final long serialVersionUID = 1;

    public IllegalActionException(String message) {
        super(message);
    }
}
