package org.library.server.service;

import lombok.RequiredArgsConstructor;
import lombok.experimental.ExtensionMethod;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.UserCreateDTO;
import org.library.server.model.dto.UserPreviewDTO;
import org.library.server.model.dto.UserUpdateDTO;
import org.library.server.model.entity.Book;
import org.library.server.model.entity.User;
import org.library.server.model.enums.UserStatus;
import org.library.server.model.mapper.UserMapper;
import org.library.server.repository.UserRepository;
import org.library.server.utility.StreamExtensions;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@ExtensionMethod({Stream.class, StreamExtensions.class})
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final BorrowService borrowService;

    @Lazy
    private final BookService bookService;

    public List<UserPreviewDTO> getUsers() {
        return this.userMapper.usersToUserPreviewDTO(this.userRepository.findByStatusCode(UserStatus.Active.value));
    }

    public void removeUser(int id) throws ResourceNotFoundException {

        User user = this.findById(id);

        this.borrowService.endBorrowsByUserId(id);

        user.setStatus(this.userMapper.userStatusToUserStatusCode(UserStatus.Inactive));

        this.userRepository.save(user);
    }

    public void removeStar(int id, int bookId) throws ResourceNotFoundException {

        Book bookToRemove = this.bookService.findById(bookId);

        User user = this.findById(id);

        user.getStarredBooks().remove(bookToRemove);

        this.userRepository.save(user);
    }

    public void addStar(int id, int bookId) throws ResourceNotFoundException {

        User user = this.findById(id);

        Book bookToAdd = this.bookService.findById(bookId);

        user.getStarredBooks().add(bookToAdd);

        this.userRepository.save(user);
    }

    public UserPreviewDTO updateUser(int id, UserUpdateDTO dto) throws ResourceNotFoundException {

        User user = this.findById(id);

        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        return this.userMapper.userToUserPreviewDTO(this.userRepository.save(user));
    }

    public UserPreviewDTO addUser(UserCreateDTO userDetails) throws IllegalActionException {

        User userToSave;

        Optional<User> user = this.userRepository.findById(userDetails.getId());

        // Creates new user when it does not exist
        if (user.isEmpty()) {
            userToSave = this.userMapper.userCreateDTOToUser(userDetails);

        } else {
            userToSave = user.get();

            // Throws error when user already exists and is active, otherwise reactivates and updates
            if (user.get().getStatus().getCode() == UserStatus.Active.value) {
                throw new IllegalActionException("User already exists for id: " + userDetails.getId());

            } else if (user.get().getStatus().getCode() == UserStatus.Inactive.value) {

                userToSave.setFirstName(userDetails.getFirstName());
                userToSave.setLastName(userDetails.getLastName());
                userToSave.setStatus(this.userMapper.userStatusToUserStatusCode(UserStatus.Active));

            }
        }

        return this.userMapper.userToUserPreviewDTO(this.userRepository.save(userToSave));
    }

    public List<UserPreviewDTO> getUsersByBook(int bookId) {
        return this.userMapper.usersToUserPreviewDTO(this.userRepository.findCurrentReadersByBookId(bookId));
    }

    protected User findById(int id) throws ResourceNotFoundException {
        return this.userRepository.findByIdAndStatusCode(id, UserStatus.Active.value)
                .orElseThrow(() -> new ResourceNotFoundException("user not found with id: " + id));
    }
}
