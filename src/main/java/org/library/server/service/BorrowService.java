package org.library.server.service;

import lombok.RequiredArgsConstructor;
import lombok.experimental.ExtensionMethod;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.BorrowCreateDTO;
import org.library.server.model.dto.BorrowDTO;
import org.library.server.model.dto.BorrowEndDTO;
import org.library.server.model.entity.BookCopy;
import org.library.server.model.entity.Borrow;
import org.library.server.model.entity.User;
import org.library.server.model.enums.BookCopyStatus;
import org.library.server.model.mapper.BookCopyMapper;
import org.library.server.model.mapper.BorrowMapper;
import org.library.server.repository.BookCopyRepository;
import org.library.server.repository.BorrowRepository;
import org.library.server.utility.StreamExtensions;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@ExtensionMethod({Stream.class, StreamExtensions.class})
@Service
@RequiredArgsConstructor
public class BorrowService {

    private final BorrowMapper borrowMapper;
    private final BookCopyMapper bookCopyMapper;

    private final BorrowRepository borrowRepository;
    private final BookCopyRepository bookCopyRepository;

    @Lazy
    private final UserService userService;

    public BorrowDTO addBorrow(BorrowCreateDTO borrowDetails) throws ResourceNotFoundException, IllegalActionException {

        if (this.bookCopyRepository.findByStatusCodeAndBookIdAndCurrentBorrowUserId(BookCopyStatus.Borrowed.value,
                        borrowDetails.getBookId(),
                        borrowDetails.getUserId())
                .isPresent()) {

                throw new IllegalActionException("Borrow for user of id: " + borrowDetails.getUserId()
                        + " and book id: " + borrowDetails.getBookId()
                        + " already exists");
        }

        User user = this.userService.findById(borrowDetails.getUserId());

        BookCopy availableCopy = this.bookCopyRepository.findTopByStatusCodeAndBookId(BookCopyStatus.AvailableToBorrow.value, borrowDetails.getBookId())
                .orElseThrow(() -> new ResourceNotFoundException("No available book copy found for book id: " + borrowDetails.getBookId()));

        Borrow borrowToAdd = new Borrow();

        borrowToAdd.setUser(user);
        borrowToAdd.setBookCopy(availableCopy);

        Borrow savedBorrow = this.borrowRepository.save(borrowToAdd);

        availableCopy.setStatus(this.bookCopyMapper.bookCopyStatusToBookCopyStatusCode(BookCopyStatus.Borrowed));
        availableCopy.setCurrentBorrow(savedBorrow);

        this.bookCopyRepository.save(availableCopy);

        return this.borrowMapper.borrowToBorrowDTO(savedBorrow);

    }

    public void endBorrow(BorrowEndDTO borrowDetails) throws ResourceNotFoundException {
        BookCopy bookCopy = this.bookCopyRepository.findByStatusCodeAndBookIdAndCurrentBorrowUserId(BookCopyStatus.Borrowed.value,
                        borrowDetails.getBookId(),
                        borrowDetails.getUserId())
                .orElseThrow(() -> new ResourceNotFoundException(("Borrow already ended or it does not exist")));

        this.removeBorrows(List.of(bookCopy));
    }

    protected void endBorrowsByUserId(int id) {
        this.removeBorrows(this.bookCopyRepository.findByStatusCodeAndCurrentBorrowUserId(BookCopyStatus.Borrowed.value, id));
    }

    private void removeBorrows(List<BookCopy> copies) {

        copies.forEach(copy -> {
            copy.setStatus(this.bookCopyMapper.bookCopyStatusToBookCopyStatusCode(BookCopyStatus.AvailableToBorrow));
            copy.setCurrentBorrow(null);
        });

        this.bookCopyRepository.saveAll(copies);
    }
}
