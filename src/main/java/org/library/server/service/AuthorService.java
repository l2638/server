package org.library.server.service;

import lombok.RequiredArgsConstructor;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.AuthorCreateDTO;
import org.library.server.model.dto.AuthorDTO;
import org.library.server.model.dto.AuthorDetailedDTO;
import org.library.server.model.dto.AuthorUpdateDTO;
import org.library.server.model.entity.Author;
import org.library.server.model.mapper.AuthorMapper;
import org.library.server.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    private final BookService bookService;

    public List<AuthorDTO> getAuthors() {
        return this.authorMapper.authorsToAuthorDTOs(this.authorRepository.findAll());
    }

    public void removeAuthor(int id) throws ResourceNotFoundException, IllegalActionException {
        Author author = this.findById(id);

        if (this.bookService.existsByAuthor(id)) {
            throw new IllegalActionException("Cannot delete author with existing books");
        }

        this.authorRepository.delete(author);
    }

    public AuthorDTO addAuthor(AuthorCreateDTO authorDTO) {
        return this.authorMapper.authorToAuthorDTO(this.authorRepository.save(this.authorMapper.authorCreateDTOtoAuthor(authorDTO)));
    }

    public AuthorDTO updateAuthor(int id, AuthorUpdateDTO authorDTO) throws ResourceNotFoundException {

        Author author = this.findById(id);

        author.setFirstName(authorDTO.getFirstName());
        author.setLastName(authorDTO.getLastName());

        return this.authorMapper.authorToAuthorDTO(this.authorRepository.save(author));

    }

    public List<AuthorDetailedDTO> getAuthorsDetailed() {
        return this.authorMapper.authorBookCountProjectionsToAuthorDetailedDTOs(this.authorRepository.getAuthorsWithBooksCount());
    }

    protected Author findById(int id) throws ResourceNotFoundException {
        return this.authorRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Author not found for id: " + id));
    }

    protected boolean existsById(int id) {
        return this.authorRepository.existsById(id);
    }
}
