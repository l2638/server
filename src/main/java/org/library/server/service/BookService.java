package org.library.server.service;

import lombok.RequiredArgsConstructor;
import lombok.experimental.ExtensionMethod;
import org.library.server.exception.IllegalActionException;
import org.library.server.exception.ResourceNotFoundException;
import org.library.server.model.dto.*;
import org.library.server.model.entity.Author;
import org.library.server.model.entity.Book;
import org.library.server.model.entity.BookCopy;
import org.library.server.model.entity.User;
import org.library.server.model.enums.BookCopyStatus;
import org.library.server.model.mapper.AuthorMapper;
import org.library.server.model.mapper.BookCopyMapper;
import org.library.server.model.mapper.BookMapper;
import org.library.server.repository.BookCopyRepository;
import org.library.server.repository.BookRepository;
import org.library.server.utility.StreamExtensions;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@ExtensionMethod({Stream.class, StreamExtensions.class})
@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final BookCopyRepository bookCopyRepository;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;
    private final BookCopyMapper bookCopyMapper;
    private final UserService userService;

    @Lazy
    private final AuthorService authorService;

    public List<BookDTO> getBooks() {
        return this.bookMapper.bookCountProjectionsToBookDTO(this.bookRepository.getBooksWithCopiesCount());
    }

    /***
     * Return a list of book details by a specific author
     * @param authorId id of the author
     * @return a list of book details
     */
    public List<BookPreviewDTO> getBookPreviews(int authorId) {
        return this.bookMapper.booksToBookPreviewsDTOs(this.bookRepository.findByAuthorId(authorId));
    }

    /***
     * Updates a book's author and number of copies
     * @param id the id of the book to update
     * @param dto contains the new author id and number of copies
     * @return details of the updated book
     * @throws ResourceNotFoundException when no author or book matches the specified ids
     * @throws IllegalActionException when trying to decrease the amount of copies of the book
     */
    public BookDTO updateBook(
            int id,
            BookUpdateDTO dto) throws ResourceNotFoundException, IllegalActionException {

        Book book = this.findById(id);

        book.setAuthor(this.bookAuthorCreateDTOToAuthor(dto.getAuthor()));

        int copiesToAddAmount = dto.getCopiesCount() - this.bookCopyRepository.countByBookId(id);

        book.getCopies().addAll(this.generateBookCopies(copiesToAddAmount, book));

        return this.bookMapper.bookToBookDTO(this.bookRepository.save(book));
    }

    public BookDTO addBook(BookCreateDTO dto) throws IllegalActionException, ResourceNotFoundException {

        Book bookToAdd = this.bookMapper.bookCreateDTOToBook(dto);

        bookToAdd.setAuthor(this.bookAuthorCreateDTOToAuthor(dto.getAuthor()));

        bookToAdd.setCopies(this.generateBookCopies(dto.getCopiesCount(), bookToAdd));

        Book book = this.bookRepository.save(bookToAdd);

        return this.bookMapper.bookToBookDTO(book);
    }

    /***
     * Creates a list of book copies associated to a book
     * @param copiesCount - the number of copies to create
     * @param book - the book the copy belongs to
     * @return a list of book copies
     * @throws IllegalActionException when the number of copies to add is negative
     */
    private List<BookCopy> generateBookCopies(int copiesCount, Book book) throws IllegalActionException {

        if (copiesCount < 0) {
            throw new IllegalActionException("Cannot decrease the number of copies of a book");
        }

        return IntStream.range(0, copiesCount)
                .mapToObj(b -> this.createBookCopy(book))
                .toList();

    }

    private BookCopy createBookCopy(Book book) {

        BookCopy copy = new BookCopy();

        copy.setStatus(this.bookCopyMapper.bookCopyStatusToBookCopyStatusCode(BookCopyStatus.AvailableToBorrow));
        copy.setBook(book);

        return copy;
    }

    /***
     * Returns a list of all the books that have copies to be borrowed
     * @return a list of book details
     */
    public List<BookPreviewDTO> getAvailableBooks() {
        return this.bookMapper.booksToBookPreviewsDTOs(this.bookRepository.findDistinctByCopiesStatusCode(BookCopyStatus.AvailableToBorrow.value));
    }

    public List<BookDetailedDTO> getBooksByUser(int userId) throws ResourceNotFoundException {

        User user = this.userService.findById(userId);

        List<Book> books = this.bookRepository.findDistinctByCopiesBorrowsUserId(userId);

        return books.stream()
                .map(book -> this.bookMapper.bookToBookDetailedDTO(book, user))
                .toList();
    }

    protected Book findById(int id) throws ResourceNotFoundException {
        return this.bookRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book not found for id: " + id));
    }

    protected boolean existsByAuthor(int authorId) {
        return this.bookRepository.existsByAuthorId(authorId);
    }

    private Author bookAuthorCreateDTOToAuthor(BookAuthorCreateDTO author) throws ResourceNotFoundException {

        if (author.getId() == null) {
            return this.authorMapper.authorCreateDTOtoAuthor(author);
        }

        return this.authorService.findById(author.getId());
    }
}
