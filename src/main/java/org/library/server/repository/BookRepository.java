package org.library.server.repository;

import org.library.server.model.projection.BookCountProjection;
import org.library.server.model.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> findByAuthorId(int authorId);

    @Query(value = "select b.id as id, b.title as title, b.author as author, b.copies.size as copiesCount from Book b")
    List<BookCountProjection> getBooksWithCopiesCount();

    List<Book> findDistinctByCopiesStatusCode(int status);

    boolean existsByAuthorId(int id);

    List<Book> findDistinctByCopiesBorrowsUserId(int id);

}
