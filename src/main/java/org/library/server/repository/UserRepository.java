package org.library.server.repository;

import org.library.server.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByIdAndStatusCode(int id, int status);

    List<User> findByStatusCode(int status);

    @Query("select u from User u, BookCopy c where c.book.id = ?1 and c.status.code = 1 and u = c.currentBorrow.user")
    List<User> findCurrentReadersByBookId(int bookId);
}
