package org.library.server.repository;

import org.library.server.model.entity.Author;
import org.library.server.model.projection.AuthorBookCountProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    @Query(value = "select a.id as id, a.firstName as firstName, a.lastName as lastName, size(a.books) as booksCount from Author a")
    List<AuthorBookCountProjection> getAuthorsWithBooksCount();
}
