package org.library.server.repository;

import org.library.server.model.entity.BookCopy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookCopyRepository extends JpaRepository<BookCopy, Integer> {

    int countByBookId(int id);

    Optional<BookCopy> findTopByStatusCodeAndBookId(int status, int bookId);

    Optional<BookCopy> findByStatusCodeAndBookIdAndCurrentBorrowUserId(int status, int bookId, int userId);

    List<BookCopy> findByStatusCodeAndCurrentBorrowUserId(int status, int userId);
}
